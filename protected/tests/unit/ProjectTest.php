<?php

class ProjectTest extends CTestCase
{
    public function testCRUD()
    {
        $newProject=new Project;
        $newProjectName = 'Test Project 2';
        $newProject->setAttributes(
            array(
                        'name'=> $newProjectName,
                        'description' => 'Test project number two',
                )
           );
        
        Yii::app()->user->setId(1);
        $this->assertTrue($newProject->save(false));
        
        $retrievedProject = Project::model()->findByPk($newProject->id);
        $this->assertTrue($retrievedProject instanceof Project);
        $this->assertEquals($newProjectName, $retrievedProject->name);
        $this->assertEquals($retrievedProject->create_user_id, 1);
        
        $newProject->name = 'Updated Test Project 2';
        $this->assertTrue($newProject->save(false));
        
        $newProjectid = $newProject->id;
        $this->assertTrue($newProject->delete());
        $deletedProject = Project::model()->findByPk($newProjectid);
        $this->assertEquals(NULL, $deletedProject);
    }
    public function testGetUserOptions()
    {
        $project = Project::model()->findByPk(2);
        
        $options = $project->userOptions;
        $this->assertTrue(is_array($options));
        $this->assertTrue(count($options)>0);
        
        print_r($options);
    }
}

?>
