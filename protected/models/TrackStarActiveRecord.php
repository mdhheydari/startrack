<?php

abstract class TrackStarActiveRecord extends CActiveRecord
{
    /**
     * Prepares create_time, update_time, create_user_id, update_user_id attributes before validation.
     */
    protected function beforeValidate()
    {
        if($this->isNewRecord)
        {
            $this->create_time = $this->update_time = date( 'Y-m-d H:i:s', time() );
            $this->create_user_id = $this->update_user_id = Yii::app()->user->id;
        }
        else
        {
            $this->update_time = date( 'Y-m-d H:i:s', time() );
            $this->update_user_id = Yii::app()->user->id;
        }

        return parent::beforeValidate();
    }
}
?>